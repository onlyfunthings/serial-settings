/* Serial parameter setting template - 2017

   Things to know...
   values of configurable parameters are stored in the parValue[] array
   Add new variable names to the parameters enum
   To set them over serial send a message of the form:
   "SX Y"
   Where 'S' denotes it a setting message
   X is parameter index (zero indexed)
   Y is parameter value

   Example: "S5 9001" sets parameter 5 to 9001
*/

/////////////////////////////////////////
//             GLOBAL VARS             //
/////////////////////////////////////////
const int serialID = 1; //used to identify specific serial devices

// TIMING
int mainUpdateRate = 30; // FPS

/////////////////////////////////////////
//              SETTINGS               //
/////////////////////////////////////////
enum parameters { //name your variables  here
  IMU_FPS,
  LED_FPS,
  NUM_PAR //keep at end and don't delete!
};

int parValue[] = { //set default values here, will be overwritten if changed over serial
  30,
  15
};

/////////////////////////////////////////
//               SETUP                 //
/////////////////////////////////////////
void setup() {
  Serial.begin(115200);
}


/////////////////////////////////////////
//               LOOP                  //
/////////////////////////////////////////

void loop() {
  receiveSerial();
}

/////////////////////////////////////////
//          OTHER FUNCTIONS            //
/////////////////////////////////////////

char incomingMsg[255] = "";
void receiveSerial() {
  if (Serial.available()) {
    memset(incomingMsg, 0, 255); //empty buffer
    while (Serial.available() > 0) {
      char inChar = Serial.read();
      size_t cur_len = strlen(incomingMsg);
      if (cur_len < 254) {
        incomingMsg[cur_len] = inChar;
        incomingMsg[cur_len + 1] = '\0';
      }
    } //done reading
    serialStates();
  }
}

void serialStates() {
  char idByte;
  memcpy(&idByte, incomingMsg, 1); // message type
  switch (idByte) {
    case '?': //Identify yourself as a responsive serial device
      Serial.print(serialID);Serial.println('!');
      break;

    case 'S': //REMOTE SETTINGS
      char newParId[10];
      char newParVal[10];
      int endPos = strchr(incomingMsg, '\0') - incomingMsg + 1;
      int spacePos = strchr(incomingMsg, ' ') - incomingMsg + 1;
      memcpy(newParId, incomingMsg + 1, spacePos - 1);
      memcpy(newParVal, strchr(incomingMsg, ' '), endPos - spacePos);

      int parameterId = atoi(newParId);
      int parameterValue = atoi(newParVal);

      if (parameterId >= NUM_PAR) { //error check
        Serial.print("oh no: Parameter out of range. You tried to set parameter ");
        Serial.print(parameterId);
        Serial.print(", but there are only ");
        Serial.print(NUM_PAR);
        Serial.println(" total!");
        break;
      }
      else {
        parValue[parameterId] = parameterValue; //update with new value
        Serial.print("Parameter: ");
        Serial.print(parameterId);
        Serial.print(" | Value: ");
        Serial.println(parameterValue);
      }
      break;
  }
}

bool timer(long* lastTime, int updateRate) { // is it time to call something?
  if (millis() - (long)*lastTime > (1000 / updateRate)) {
    *lastTime = millis();
    return true;
  }
  return false;
}
