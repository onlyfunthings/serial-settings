# README #

Tired of hardcoding values into firmware? me too. This sketch lets you send commands over serial to change your parameters on the fly. 

### Things to know... ###
* Values of configurable parameters are stored in the parValue[] array
* Add new variable names to the parameters enum

### Usage ###
* To set values over serial, send a message of the form:
*   "S5 9001"
*   Where 'S' denotes it a setting message
*   "5" is parameter index (zero indexed)
*   "9001" is the parameter value